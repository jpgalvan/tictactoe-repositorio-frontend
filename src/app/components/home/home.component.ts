import { Component } from '@angular/core';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent{

  //Turno del jugador
  player: boolean = true;
  //Iniciar juego
  game: boolean = false;

  //Combinacion ganadora
  jugadorX: number[] = [];
  jugadorO: number[] = [];
  combinaciones = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9],
    [1, 4, 7],
    [2, 5, 8],
    [3, 6, 9],
    [1, 5, 9],
    [3, 5, 7]
  ];


  constructor() { }

  //inicia el juego
  startGame() {
    this.game = !this.game;
  }

  //en cada click, guarda la celda presionada en su pila correspondiente X ó O
  onCellClick(e) {
    var buttonClicked = document.getElementById(e.target.getAttribute('id')) as HTMLButtonElement;
    var letter;
    if (this.player) {
      letter = "X"
      this.jugadorX.push(+e.target.getAttribute('id'));
      this.validate(this.jugadorX, letter);
    }
    else {
      letter = "O"
      this.jugadorO.push(+e.target.getAttribute('id'));
      this.validate(this.jugadorO, letter);
    }
    buttonClicked.innerText = letter
    buttonClicked.disabled = true;
    this.player = !this.player;
  }


  validate(combinacionJugador: any, letter) {
    //si hay mas de 5 movimientos entonces se declara empate
    if (combinacionJugador.length >= 5) {
      Swal.fire({
        title: 'Juego terminado',
        text: "Empate, ¿Empezar de nuevo?",
        icon: 'error',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si',
        cancelButtonText: 'No'
      }).then((result) => {(result.value) ? this.restart(true) : this.restart(false)});
    }
    else {
      //itera para cada una de las posibles combinaciones ganadoras y comprueba si la pila del jugador esta en una de ellas
      this.combinaciones.map((combinacion) => {
        if ((combinacionJugador.includes(combinacion[0])) && (combinacionJugador.includes(combinacion[1])) && (combinacionJugador.includes(combinacion[2]))) {
          Swal.fire({
            title: 'Juego terminado',
            text: "ganó el jugador '" + letter + "', ¿Empezar de nuevo?",
            icon: 'success',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si',
            cancelButtonText: 'No'
          }).then((result) => {(result.value) ? this.restart(true) : this.restart(false)});
        }
      })
    }
  }



  //reinicia las variables y limpia los bottones
  restart(opt) {
    for (let i = 1; i <= 9; i++) {
      var btn = document.getElementById('' + i) as HTMLButtonElement
      btn.innerText = "";
      btn.disabled = false;
    }
    this.game = opt;
    this.jugadorO = [];
    this.jugadorX = [];
  }

}
