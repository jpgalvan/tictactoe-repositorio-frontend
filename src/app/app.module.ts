import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RootComponent } from './components/root/root.component';


import { FrameworkModule } from '@next/nx-core';
import { CommonsModule } from '@next/nx-controls-common';


import { MatSliderModule } from '@angular/material/slider';
import { MatCardModule } from '@angular/material';

const config = {
  usernameLabel: 'BRM15484',
  usernamePlaceholder: 'JavierPiedra',
  endpoint: '/api',
  application: 'MI_PROYECTO',
  applicationTitle: 'Mi Proyecto'
};

@NgModule({
  declarations: [
    AppComponent,
    RootComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    //FrameworkModule.forRoot(config),
    CommonsModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
